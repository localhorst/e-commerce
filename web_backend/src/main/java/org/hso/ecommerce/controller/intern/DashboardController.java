package org.hso.ecommerce.controller.intern;

import org.hso.ecommerce.entities.dashboard.DashboardSummary;
import org.hso.ecommerce.repos.dashboard.DashboardSummaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DashboardController {

    @Autowired
    private DashboardSummaryRepository dashboardSummaryRepository;

    @GetMapping("/intern/dashboardsummary")
    public List<DashboardSummary> getDashboardEntries()
    {
        List<DashboardSummary> inTimespan = dashboardSummaryRepository.findInTimespan(PageRequest.of(0, 7) );
        return inTimespan;
    }

}
