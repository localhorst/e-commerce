package org.hso.ecommerce.entities.shop;

import javax.persistence.*;

@Entity
@Table(name = "customer_order_positions")
public class CustomerOrderPosition {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic
    public long id;

    @ManyToOne(optional = false)
    public CustomerOrder order;

    @ManyToOne(optional = false)
    public Article article;

    public int pricePerUnit;
    public int quantity;

    public int getSumPrice(){
        return article.getPriceGross() * quantity;
    }
}
