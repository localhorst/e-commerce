package org.hso.ecommerce.repos.shop;

import org.hso.ecommerce.entities.shop.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {

	@Query("SELECT i.id FROM Image i WHERE i.path = :path")
	Optional<Integer> findImageIDByPath(@Param("path") String path);

	@Query("SELECT i FROM Image i WHERE i.id = :imageId")
	Image findImageById(@Param("imageId") long imageId);

}
