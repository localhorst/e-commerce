package org.hso.ecommerce.entities.shop;

import org.hso.ecommerce.entities.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "customer_orders")
public class CustomerOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic
    public long id;

    @ManyToOne(optional = false)
    public User customer;

    @Embedded
    public Address destination;

    @OneToMany(
            targetEntity = CustomerOrderPosition.class,
            mappedBy = "order", cascade = CascadeType.ALL
    )
    public List<CustomerOrderPosition> positions = new ArrayList<>();

    @NotNull
    public java.sql.Timestamp created;

    @Column(nullable = true)
    public String trackingId;

    @Column(nullable = true)
    public java.sql.Timestamp inDeliverySince;

    @Column(nullable = true)
    public java.sql.Timestamp deliveredAt;

    public int totalNetCent;
    public int totalGrossCent;
    public int totalVatCent;

    public String formatInDeliverySince(){
        return new SimpleDateFormat("dd.MM.yyyy HH:mm").format(inDeliverySince);
    }

    public String formatCreated(){
        return new SimpleDateFormat("dd.MM.yyyy HH:mm").format(created);
    }

    public String formatDeliveredAt(){
        return new SimpleDateFormat("dd.MM.yyyy HH:mm").format(deliveredAt);
    }
}
