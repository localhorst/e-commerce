package org.hso.ecommerce.controller.intern.suppliers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hso.ecommerce.controller.intern.accounting.AccountingController;
import org.hso.ecommerce.controller.intern.accounting.AccountingController.ShortTemplateBooking;
import org.hso.ecommerce.controller.intern.accounting.AccountingController.ShortTemplateBookingResult;
import org.hso.ecommerce.entities.booking.Booking;
import org.hso.ecommerce.entities.supplier.Supplier;
import org.hso.ecommerce.entities.supplier.SupplierOrder;
import org.hso.ecommerce.repos.booking.BookingRepository;
import org.hso.ecommerce.repos.supplier.SupplierOrderRepository;
import org.hso.ecommerce.repos.supplier.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/intern/")
public class SupplierIndexController {

	@Autowired
	private final SupplierRepository supplierRepository = null;

	@Autowired
	private final SupplierOrderRepository supplierOrderRepository = null;

	@Autowired
	private final BookingRepository bookingRepository = null;

	@Autowired
	private final AccountingController accountingController = null;

	@GetMapping("suppliers")
	public String listSuppliers(Model model) {

		List<UImodelSuppliers> totals = new ArrayList<UImodelSuppliers>();

		for (Supplier supplier : supplierRepository.findAll()) {
			UImodelSuppliers tmp = new UImodelSuppliers(supplier.id, supplier.name);
			totals.add(tmp);
		}

		model.addAttribute("suppliers", totals);
		return "intern/suppliers/index";
	}

	@GetMapping("/suppliers/{id}")
	public String supplierDetail(Model model, @PathVariable String id) {

		long supplierId = Long.parseLong(id);

		// add orders from supplier to UImodel
		List<UImodelSupplierDetailOrders> orders = new ArrayList<UImodelSupplierDetailOrders>();
		for (SupplierOrder supplierOrder : supplierOrderRepository.findOrderBySupplierID(supplierId)) {
			orders.add(new UImodelSupplierDetailOrders(supplierOrder));
		}

		// Table of bookings
		List<Booking> bookings = bookingRepository.supplierBookingsReverseChronologically(supplierId);
		ShortTemplateBookingResult bookingResult = accountingController.buildShortTemplate(bookings,
				account -> account.supplierAccount != null && account.supplierAccount.id == supplierId);

		UImodelSupplierDetail total = new UImodelSupplierDetail(supplierRepository.findSupplierById(supplierId).name,
				bookingResult.balance, orders, bookingResult.bookings);

		model.addAttribute("SupplierDetail", total);

		return "intern/suppliers/id";
	}

	public class UImodelSuppliers {
		public long id;
		public String name;

		public UImodelSuppliers(long id, String name) {
			this.id = id;
			this.name = name;
		}

	}

	public class UImodelSupplierDetail {

		public String name;
		public String balance;
		public List<UImodelSupplierDetailOrders> orders;
		public List<ShortTemplateBooking> bookings;

		public UImodelSupplierDetail(String name, String balance, List<UImodelSupplierDetailOrders> orders,
				List<ShortTemplateBooking> bookings
		) {
			this.name = name;
			this.balance = balance;
			this.orders = orders;
			this.bookings = bookings;
		}

	}

	public class UImodelSupplierDetailOrders {
		public long id;
		public String dateOrder;
		public String articleName;
		public long articleId;
		public String priceNet;
		public String quantity;
		public String priceTotal;
		public boolean arrived;

		public UImodelSupplierDetailOrders(SupplierOrder order) {
			this.id = order.id;
			this.articleName = order.ordered.title;
			this.articleId = order.ordered.id;
			this.priceNet = String.format("%.2f", ((float) order.pricePerUnitNetCent / 100));
			this.quantity = String.valueOf(order.numberOfUnits);
			this.priceTotal = String.format("%.2f", ((float) order.totalPriceNet / 100));

			Date date = new Date();
			date.setTime(order.created.getTime());
			this.dateOrder = new SimpleDateFormat("dd.MM.yyyy").format(date);

			if (order.delivered != null) {
				arrived = true;
			} else {
				arrived = false;
			}
		}
	}

	public class UImodelSupplierDetailBookings {

		public String dateBooking;
		public String price;
		public String srcName;
		public String balance;
		public String reason;
		public long orderID;

		public UImodelSupplierDetailBookings(Booking booking) {
			Date date = new Date();
			date.setTime(booking.reason.supplierOrder.created.getTime());
			this.dateBooking = new SimpleDateFormat("dd.MM.yyyy").format(date);
			this.price = String.format("%.2f", ((float) booking.amountCent / 100));
			this.srcName = ((booking.source.isMainAccount) ? "Hauptkonto" : booking.source.supplierAccount.name);
			this.balance = String.format("%.2f", ((float) booking.destination.newSumCent / 100));
			this.reason = booking.reason.comment;
			this.orderID = booking.reason.supplierOrder.id;
		}
	}
}
