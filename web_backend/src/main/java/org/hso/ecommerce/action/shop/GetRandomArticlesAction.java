package org.hso.ecommerce.action.shop;

import org.hso.ecommerce.entities.shop.Article;

import java.util.ArrayList;
import java.util.List;

public class GetRandomArticlesAction {

    public static List<Article> getRandomArticles(int quantity, List<Article> advertisedArticles) {
        List<Article> randomisedArticles = new ArrayList<Article>();
        int loopcount = Math.min(quantity, advertisedArticles.size());
        for (int i = 0; i < loopcount; i++) {
            int index = (int) (Math.random() * advertisedArticles.size());
            randomisedArticles.add(advertisedArticles.remove(index));
        }
        return randomisedArticles;
    }
}
