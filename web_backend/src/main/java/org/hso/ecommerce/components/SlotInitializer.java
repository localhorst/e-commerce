package org.hso.ecommerce.components;

import org.hso.ecommerce.app.config.AppSettings;
import org.hso.ecommerce.entities.warehouse.Slot;
import org.hso.ecommerce.repos.warehouse.SlotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class SlotInitializer {

	@Autowired
	private final SlotRepository slotRepository = null;

    @Autowired
    private final AppSettings appSettings = null;

    @PostConstruct
    public void init() {
        int NUM_SLOTS = appSettings.getNumberOfStorageSpaces();

        for (int i = 1; i <= NUM_SLOTS; i++) {
            if (!slotRepository.findBySlotNum(i).isPresent()) {
                Slot slotAdded = new Slot();
                slotAdded.slotNum = i;
                slotRepository.save(slotAdded);

                System.out.println("Added Slot " + i + " to DB");
            }
        }
    }

}
