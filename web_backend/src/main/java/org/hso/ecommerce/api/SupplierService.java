package org.hso.ecommerce.api;

import org.hso.ecommerce.api.data.Order;
import org.hso.ecommerce.api.data.OrderConfirmation;
import org.hso.ecommerce.api.data.Supplier;
import org.springframework.web.client.RestTemplate;

public class SupplierService {

    private final String url;

    public SupplierService(String url) {
        this.url = url;
    }

    public Supplier getSupplier() {
        RestTemplate restTemplate = new RestTemplate();

        return restTemplate.getForObject(url, Supplier.class);
    }

    public OrderConfirmation order(Order order) {
        RestTemplate restTemplate = new RestTemplate();

        return restTemplate.postForObject(url + "/order", order, OrderConfirmation.class);
    }
}
