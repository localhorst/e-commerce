package org.hso.ecommerce.action.booking;

import org.hso.ecommerce.entities.booking.Booking;
import org.hso.ecommerce.entities.booking.BookingAccountEntry;
import org.hso.ecommerce.entities.booking.BookingReason;

public class CreateBookingAction {

    private Booking booking;

    public CreateBookingAction(BookingAccountEntry source, BookingAccountEntry destination, BookingReason reason, int amountCent) {
        booking = new Booking();
        booking.created = new java.sql.Timestamp(System.currentTimeMillis());
        booking.reason = reason;
        booking.amountCent = amountCent;

        assert source != null || destination != null;

        if (source != null) {
            booking.source = source.copyAddAmount(-amountCent);
        }
        if (destination != null) {
            booking.destination = destination.copyAddAmount(amountCent);
        }

    }

    public Booking finish() {
        return booking;
    }
}
