package org.hso.ecommerce.api.data;

public class Article {
    public String title;
    public String manufacturer;
    public String articleNumber;

    public int vatPercent;
    public int pricePerUnitNet;
    public boolean shouldBeAdvertised;
}
