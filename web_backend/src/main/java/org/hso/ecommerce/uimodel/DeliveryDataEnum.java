package org.hso.ecommerce.uimodel;

public enum DeliveryDataEnum {
    OK,
    NO_TRACKING_ID,
    NO_DATA
}
