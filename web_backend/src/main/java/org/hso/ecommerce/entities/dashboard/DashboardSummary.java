package org.hso.ecommerce.entities.dashboard;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "dashboard_summaries")
public class DashboardSummary {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic
    public long id;

    @NotNull
    public java.sql.Date created;

    @NotNull
    public Integer todaysNewCustomers;

    @NotNull
    public Integer todaysCustomersOrders;

    @NotNull
    public Integer todaysSalesCent;

    @NotNull
    public Double currentWarehouseCapacity;

    @NotNull
    public Double todaysWarehouseCapacity;
}
