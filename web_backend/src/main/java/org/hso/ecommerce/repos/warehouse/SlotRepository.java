package org.hso.ecommerce.repos.warehouse;

import org.hso.ecommerce.entities.warehouse.Slot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SlotRepository extends JpaRepository<Slot, Long> {

    @Query("SELECT s FROM Slot s WHERE s.slotNum = :slotNum")
    Optional<Slot> findBySlotNum(int slotNum);


}
