package org.hso.ecommerce.entities.supplier;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "article_offers")
public class ArticleOffer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic
    public long id;

    @NotNull
    public String manufacturer;
    
    @NotNull
    public String title;
    
    @NotNull
    public int pricePerUnitNet;

    @NotNull
    public String articleNumber;

    public int vatPercent;

    public boolean shouldBeAdvertised;
    
    @ManyToOne(optional = true)
    public Supplier cheapestSupplier;
}
