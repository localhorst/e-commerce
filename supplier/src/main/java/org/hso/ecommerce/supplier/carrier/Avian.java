package org.hso.ecommerce.supplier.carrier;

import java.time.LocalDateTime;
import java.util.Random;

public class Avian implements Carrier {
    @Override
    public String getName() {
        return "Avian Carriers";
    }

    @Override
    public String generateTrackingId() {
        Random rnd = new Random();

        return "2001-"
                + Integer.toHexString(rnd.nextInt(0xFFFF))
                + "--"
                + Integer.toHexString(rnd.nextInt(0xFFFF))
                + "-"
                + Integer.toHexString(rnd.nextInt(0xFFFF));
    }

    @Override
    public LocalDateTime arrivalEstimate() {
        return LocalDateTime.now().plusHours(8);
    }
}
