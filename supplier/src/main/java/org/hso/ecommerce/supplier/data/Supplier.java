package org.hso.ecommerce.supplier.data;

import java.util.List;

public class Supplier {

    public String id;
    public String name;
    public SupplierDiscount discount;
    public List<Article> articles;

    public Article findArticle(String manufacturer, String articleNumber) {
        for(Article a : articles) {
            if(a.manufacturer.equals(manufacturer) && a.articleNumber.equals(articleNumber)) {
                return a;
            }
        }

        return null;
    }
}
