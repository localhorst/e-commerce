package org.hso.ecommerce.supplier;


import org.hso.ecommerce.supplier.data.Delivery;
import org.hso.ecommerce.supplier.data.DeliveryManager;
import org.hso.ecommerce.supplier.data.ReturnStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class RequestController {



    @PostMapping("/newDelivery")
    public String supplier(HttpServletResponse response, HttpServletRequest request, @RequestBody Delivery delivery) {
        DeliveryManager.getInstance().add(delivery);

        return delivery.getUuid();
    }

    @GetMapping(value = "/status", produces = MediaType.APPLICATION_JSON_VALUE)
    public ReturnStatus searchArticles(@RequestParam(value = "trackingID") String trackingID, HttpServletRequest request, HttpServletResponse response) {

        Delivery delivery = DeliveryManager.getInstance().getDeliveryByID(trackingID);
        if (delivery == null) {
            Delivery lostDelivery = Delivery.lostDelivery(trackingID);
            DeliveryManager.getInstance().add(lostDelivery);
            delivery = lostDelivery;
        }

        return new ReturnStatus(delivery.getStatus(), delivery.getEstimatedArrival());
    }
}
