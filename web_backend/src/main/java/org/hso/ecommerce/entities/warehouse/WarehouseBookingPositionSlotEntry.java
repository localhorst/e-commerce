package org.hso.ecommerce.entities.warehouse;

import org.hso.ecommerce.entities.shop.Article;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "warehouse_booking_position_entries")
public class WarehouseBookingPositionSlotEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic
    public long id;

    @NotNull
    @ManyToOne
    public Article article;

    @NotNull
    public int newSumSlot;

    @NotNull
    @ManyToOne
    public Slot slot;

    public WarehouseBookingPositionSlotEntry copyAddAmount(int amount, Article article) {
        // Article can be changed if newSumSlot == 0.
        if (this.newSumSlot != 0 && this.article.id != article.id) {
            throw new IllegalArgumentException("Article does not match.");
        }

        WarehouseBookingPositionSlotEntry e = new WarehouseBookingPositionSlotEntry();

        e.article = article;
        e.slot = slot;

        e.newSumSlot = newSumSlot + amount;

        assert e.article.warehouseUnitsPerSlot >= e.newSumSlot;
        assert e.newSumSlot >= 0;

        return e;
    }

    public static WarehouseBookingPositionSlotEntry empty(Article article, Slot slot) {
        WarehouseBookingPositionSlotEntry e = new WarehouseBookingPositionSlotEntry();

        e.article = article;
        e.slot = slot;
        e.newSumSlot = 0;

        return e;
    }
}
