package org.hso.ecommerce.entities.booking;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "bookings")
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic
    public long id;

    // always >= 0
    public int amountCent;

    @NotNull
    public java.sql.Timestamp created;

    @ManyToOne(optional = true, cascade = CascadeType.ALL)
    public BookingAccountEntry source;

    @ManyToOne(optional = true, cascade = CascadeType.ALL)
    public BookingAccountEntry destination;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    public BookingReason reason;
}
