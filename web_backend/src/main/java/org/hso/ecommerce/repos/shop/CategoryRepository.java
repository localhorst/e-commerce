package org.hso.ecommerce.repos.shop;

import org.hso.ecommerce.entities.shop.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

	@Query("SELECT a FROM Category a WHERE a.name = :name")
	Optional<Category> findCategoryByName(@Param("name") String name);

    @Query("SELECT c FROM Category c")
    List<Category> getCategories();
}