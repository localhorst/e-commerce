package org.hso.ecommerce.entities.cron;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "background_jobs")
public class BackgroundJob {

    public static final String JOB_DASHBOARD = "Dashboard";
    public static final String JOB_REORDER = "SupplierOrder";
    public static final String JOB_SUPPLIER_AUTO_PAYMENT = "SupplierAutoPayment";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic
    public long id;

    @NotNull
    public String jobName;

    @NotNull
    public java.sql.Timestamp lastExecution;
}
