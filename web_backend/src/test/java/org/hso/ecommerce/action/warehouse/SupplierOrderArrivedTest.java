package org.hso.ecommerce.action.warehouse;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.hso.ecommerce.action.warehouse.SupplierOrderArrivedAction.NoSpaceInWarehouseException;
import org.hso.ecommerce.entities.shop.Article;
import org.hso.ecommerce.entities.supplier.SupplierOrder;
import org.hso.ecommerce.entities.warehouse.Slot;
import org.hso.ecommerce.entities.warehouse.WarehouseBooking;
import org.hso.ecommerce.entities.warehouse.WarehouseBookingPositionSlotEntry;
import org.junit.jupiter.api.Test;

class SupplierOrderArrivedTest {

	@Test
	void test() throws NoSpaceInWarehouseException {
		Article article = new Article();
		article.warehouseUnitsPerSlot = 15;

		List<WarehouseBookingPositionSlotEntry> candidates = new ArrayList<>();
		addCandidate(candidates, article, 3, 13);
		addCandidate(candidates, article, 6, 12);
		addCandidate(candidates, article, 7, 0);
		addCandidate(candidates, article, 10, 14);
		addCandidate(candidates, article, 12, 11);
		addCandidate(candidates, article, 16, 0);
		SupplierOrder order = new SupplierOrder();
		order.numberOfUnits = 17;

		SupplierOrderArrivedAction.Result result = new SupplierOrderArrivedAction(candidates, order, article).finish();
		WarehouseBooking booking = result.getBooking();

		assertEquals(10, booking.positions.get(0).slotEntry.slot.slotNum);
		assertEquals(1, booking.positions.get(0).amount);
		assertEquals(3, booking.positions.get(1).slotEntry.slot.slotNum);
		assertEquals(2, booking.positions.get(1).amount);
		assertEquals(6, booking.positions.get(2).slotEntry.slot.slotNum);
		assertEquals(3, booking.positions.get(2).amount);
		assertEquals(12, booking.positions.get(3).slotEntry.slot.slotNum);
		assertEquals(4, booking.positions.get(3).amount);
		assertEquals(7, booking.positions.get(4).slotEntry.slot.slotNum);
		assertEquals(7, booking.positions.get(4).amount);
	}
	
	private void addCandidate(List<WarehouseBookingPositionSlotEntry> candidates, Article article, int slotNum, int amount) {
		WarehouseBookingPositionSlotEntry entry = new WarehouseBookingPositionSlotEntry();
		entry.article = article;
		entry.slot = new Slot();
		entry.slot.slotNum = slotNum;
		entry.newSumSlot = amount;
		candidates.add(entry);
	}

}
