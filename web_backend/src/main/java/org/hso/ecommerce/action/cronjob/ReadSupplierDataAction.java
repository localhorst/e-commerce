package org.hso.ecommerce.action.cronjob;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import org.hso.ecommerce.api.SupplierService;
import org.hso.ecommerce.api.data.Article;
import org.hso.ecommerce.api.data.Supplier;

public class ReadSupplierDataAction {
    private List<org.hso.ecommerce.entities.supplier.Supplier> suppliers;

    public static class ArticleIdentifier {
        public final String manufacturer;
        public final String articleNumber;

        public ArticleIdentifier(String manufacturer, String articleNumber) {
            this.manufacturer = manufacturer;
            this.articleNumber = articleNumber;
        }

        @Override
        public int hashCode() {
            return Objects.hash(manufacturer, articleNumber);
        }

        @Override
        public boolean equals(Object other) {
            if (!(other instanceof ArticleIdentifier)) {
                return false;
            }
            ArticleIdentifier otherId = (ArticleIdentifier) other;
            return this.manufacturer.equals(otherId.manufacturer) && this.articleNumber.equals(otherId.articleNumber);
        }
    }

    public ReadSupplierDataAction(List<org.hso.ecommerce.entities.supplier.Supplier> suppliers) {
        this.suppliers = suppliers;
    }

    public static class Offer {
        public final org.hso.ecommerce.entities.supplier.Supplier dbSupplier;
        public final Supplier apiSupplier;

        public Offer(org.hso.ecommerce.entities.supplier.Supplier dbSupplier, Supplier apiSupplier) {
            this.dbSupplier = dbSupplier;
            this.apiSupplier = apiSupplier;
        }
    }

    public static class Result {
        public final ArrayList<Supplier> supplierData;
        public final HashMap<ArticleIdentifier, Offer> cheapestOffer;

        public Result(ArrayList<Supplier> supplierData, HashMap<ArticleIdentifier, Offer> cheapestOffer) {
            this.supplierData = supplierData;
            this.cheapestOffer = cheapestOffer;
        }
    }

    public Result finish() {
        ArrayList<Supplier> suppliers = new ArrayList<>();
        HashMap<ArticleIdentifier, Integer> price = new HashMap<>();
        HashMap<ArticleIdentifier, Offer> cheapest = new HashMap<>();
        for (org.hso.ecommerce.entities.supplier.Supplier supplier : this.suppliers) {
            SupplierService service = new SupplierService(supplier.apiUrl);
            Supplier apiSupplier = service.getSupplier();
            suppliers.add(apiSupplier);
            for (Article article : apiSupplier.articles) {
                ArticleIdentifier identifier = new ArticleIdentifier(article.manufacturer, article.articleNumber);
                Integer previousPrice = price.get(identifier);
                if (previousPrice == null || article.pricePerUnitNet < previousPrice) {
                    price.put(identifier, article.pricePerUnitNet);
                    cheapest.put(identifier, new Offer(supplier, apiSupplier));
                }
            }
        }

        return new Result(suppliers, cheapest);
    }
}
