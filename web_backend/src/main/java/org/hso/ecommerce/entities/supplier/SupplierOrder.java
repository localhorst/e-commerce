package org.hso.ecommerce.entities.supplier;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Entity
@Table(name = "supplier_orders")
public class SupplierOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic
    public long id;

    @NotNull
    public java.sql.Timestamp created;

    @ManyToOne(optional = false)
    public Supplier supplier;

    @ManyToOne(optional = false)
    public ArticleOffer ordered;

    public int numberOfUnits;
    public int pricePerUnitNetCent;

    // Includes discounts
    public int totalPriceNet;

    @Column(nullable = true)
    public String carrier;

    @Column(nullable = true)
    public String trackingId;

    @Column(nullable = true)
    public Timestamp estimatedArrival;

    @Column(nullable = true)
    public Timestamp delivered;

    public boolean wasDelivered() {
        return delivered != null;
    }
}
