package org.hso.ecommerce.repos.supplier;

import org.hso.ecommerce.entities.supplier.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SupplierRepository extends JpaRepository<Supplier, Long> {

	@Query("SELECT a FROM Supplier a")
	List<Supplier> findAll();

	@Query("SELECT a FROM Supplier a WHERE a.id = :supplierId")
	Supplier findSupplierById(@Param("supplierId") long supplierId);

	@Query("SELECT a FROM Supplier a WHERE a.uuid = :uuid")
	Optional<Supplier> findByUuid(@Param("uuid") String uuid);
}