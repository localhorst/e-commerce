package org.hso.ecommerce.action.shop;

import org.hso.ecommerce.api.RestServiceForDelivery;
import org.hso.ecommerce.entities.shop.CustomerOrder;
import org.springframework.web.client.ResourceAccessException;

import java.sql.Timestamp;
import java.util.Date;

public class EnableTrackingAction {

    public static void addTrackingInfo(RestServiceForDelivery deliveryService, CustomerOrder customerOrder) throws ResourceAccessException {
        customerOrder.inDeliverySince = new Timestamp(new Date().getTime());
        customerOrder.trackingId = deliveryService.getDeliveryID(customerOrder);
    }
}
