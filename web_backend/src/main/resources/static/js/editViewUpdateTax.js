document.addEventListener("DOMContentLoaded", function() {
    let priceElm = document.getElementById("price");
    let priceGrossElm = document.getElementById("priceGross");
    let vatPercent = parseInt(document.getElementById("vatPercent").value);

    let updateFn = () => {
       let net = Math.floor(priceElm.value*100);
       let vat = Math.floor((net * vatPercent) / 100);
       let gross = net + vat;

       priceGrossElm.innerText = (gross / 100.0).toFixed(2).replace("\\.", ",");
    };

    priceElm.onchange = updateFn;
});
