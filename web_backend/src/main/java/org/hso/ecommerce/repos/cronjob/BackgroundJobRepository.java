package org.hso.ecommerce.repos.cronjob;

import org.hso.ecommerce.entities.cron.BackgroundJob;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BackgroundJobRepository extends JpaRepository<BackgroundJob, Long> {

}
