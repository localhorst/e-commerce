package org.hso.ecommerce.repos.supplier;

import org.hso.ecommerce.entities.supplier.ArticleOffer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArticleOfferRepository extends JpaRepository<ArticleOffer, Long> {

}
