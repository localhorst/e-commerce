package org.hso.ecommerce.uimodel;

import com.fasterxml.jackson.annotation.JsonCreator;


public class DeliveryData
{

    private final String status;

    private final DeliveryDataEnum deliveryDataEnum;

    private final String estimatedArrival;

    private boolean isDelivered;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public DeliveryData(String status, String estimatedArrival) {
        this.status = status;
        this.estimatedArrival = estimatedArrival;
        this.deliveryDataEnum = DeliveryDataEnum.OK;
        isDelivered = status.equals("Lieferung erfolgreich");
    }

    public DeliveryData(String status, String estimatedArrival, DeliveryDataEnum deliveryDataEnum) {
        this.status = status;
        this.estimatedArrival = estimatedArrival;
        this.deliveryDataEnum = deliveryDataEnum;
        isDelivered = status.equals("Lieferung erfolgreich");
    }

    public boolean isDelivered() {
        return isDelivered;
    }

    public String getStatus() {
        return status;
    }

    public String getEstimatedArrival() {
        return estimatedArrival;
    }

    public boolean allOk() {
        return deliveryDataEnum == DeliveryDataEnum.OK;
    }

    public boolean noTrackingID() {
        return deliveryDataEnum == DeliveryDataEnum.NO_TRACKING_ID;
    }

    public boolean noData() {
        return deliveryDataEnum == DeliveryDataEnum.NO_DATA;
    }

}

