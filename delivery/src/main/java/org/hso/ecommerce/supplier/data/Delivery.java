package org.hso.ecommerce.supplier.data;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class Delivery {

    private String[] states = {"Bestellung eingegangen", "Bestellung auf dem Weg", "Lieferung erfolgreich"};
    private int[] timeBorder = {4, 24};

    private String name;
    private String address;

    // Why is this a string and creationTime a Date?!
    private String estimatedArrival;

    private Date creationTime;
    private String uuid;

    public Delivery(String name, String address) {
        this.name = name;
        this.address = address;
        this.uuid = UUID.randomUUID().toString();
        this.creationTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        this.estimatedArrival = formatter.format(addDays((Date) this.creationTime.clone(), 1));
    }

    public static Delivery lostDelivery(String uuid) {
        Delivery delivery = new Delivery("", "");
        delivery.uuid = uuid;
        delivery.creationTime = addDays(new Date(), -1);
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        delivery.estimatedArrival = formatter.format(addDays((Date) delivery.creationTime.clone(), 1));
        return delivery;
    }

    public String getStatus() {
        Date now = new Date();
        long timeNow = now.getTime();
        long creationTime = this.creationTime.getTime();

        // Wow, that's how calculate date diffs.
        long diff = timeNow - creationTime;
        double hour = (((diff / 1000.0) / 3600.0));

        for (int i = 0; i < timeBorder.length; i++) {

            if (hour < timeBorder[i])
                return states[i];
        }

        return states[timeBorder.length];
    }

    public String getEstimatedArrival() {

        return estimatedArrival;
    }

    private static Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }

    public String getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }
}
