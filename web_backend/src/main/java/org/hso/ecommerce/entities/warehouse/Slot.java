package org.hso.ecommerce.entities.warehouse;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "warehouse_slots")
public class Slot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic
    public long id;

    @NotNull
    @Column(unique = true)
    public int slotNum;
}
