package org.hso.ecommerce.repos.warehouse;

import org.hso.ecommerce.entities.warehouse.WarehouseBooking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WarehouseBookingRepository extends JpaRepository<WarehouseBooking, Long> {

    @Query("Select b FROM WarehouseBooking b WHERE b.isDone = 0")
    List<WarehouseBooking> findNotDone();

    @Query("Select b FROM WarehouseBooking b ORDER BY b.id DESC")
    List<WarehouseBooking> findAllDesc();

}

