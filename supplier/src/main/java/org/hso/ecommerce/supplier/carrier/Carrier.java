package org.hso.ecommerce.supplier.carrier;

import java.time.LocalDateTime;

public interface Carrier {
    public String getName();

    public String generateTrackingId();

    public LocalDateTime arrivalEstimate();
}

