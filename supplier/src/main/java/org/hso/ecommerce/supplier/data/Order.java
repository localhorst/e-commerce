package org.hso.ecommerce.supplier.data;

public class Order {
    public String manufacturer;
    public String articleNumber;

    public int quantity;
    public int maxTotalPriceCentNet;
}
