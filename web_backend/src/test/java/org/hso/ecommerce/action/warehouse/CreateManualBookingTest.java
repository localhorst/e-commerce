package org.hso.ecommerce.action.warehouse;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.hso.ecommerce.action.warehouse.CreateManuelBookingAction.ArticleSlotConstraintFailedException;
import org.hso.ecommerce.entities.shop.Article;
import org.hso.ecommerce.entities.warehouse.Slot;
import org.hso.ecommerce.entities.warehouse.WarehouseBooking;
import org.hso.ecommerce.entities.warehouse.WarehouseBookingPositionSlotEntry;
import org.junit.jupiter.api.Test;

class CreateManualBookingTest {

	@Test
	void testIncrease() throws ArticleSlotConstraintFailedException {
		Article article = new Article();
		article.warehouseUnitsPerSlot = 20;
		article.id = 1;
		Article previousArticle = new Article();
		previousArticle.id = 2;
		WarehouseBookingPositionSlotEntry destinationSlot = new WarehouseBookingPositionSlotEntry();
		destinationSlot.slot = new Slot();
		destinationSlot.slot.slotNum = 5;
		destinationSlot.article = previousArticle;
		Optional<WarehouseBookingPositionSlotEntry> optSourceSlot = Optional.empty();
		
		WarehouseBooking result = new CreateManuelBookingAction(article, 17, optSourceSlot, Optional.of(destinationSlot), "some reason").finish();
		
		assertEquals(17, result.positions.get(0).amount);
		assertEquals(17, result.positions.get(0).slotEntry.newSumSlot);
		assertEquals(5, result.positions.get(0).slotEntry.slot.slotNum);
		assertTrue(result.reason.isManuel);
		assertEquals("some reason", result.reason.comment);
	}

	@Test
	void testDecrease() throws ArticleSlotConstraintFailedException {
		Article article = new Article();
		article.warehouseUnitsPerSlot = 20;
		article.id = 1;
		WarehouseBookingPositionSlotEntry sourceSlot = new WarehouseBookingPositionSlotEntry();
		sourceSlot.article = article;
		sourceSlot.slot = new Slot();
		sourceSlot.slot.slotNum = 8;
		sourceSlot.newSumSlot = 17;
		Optional<WarehouseBookingPositionSlotEntry> optDestinationSlot = Optional.empty();
		
		WarehouseBooking result = new CreateManuelBookingAction(article, 13, Optional.of(sourceSlot), optDestinationSlot, "some other reason").finish();
		
		assertEquals(-13, result.positions.get(0).amount);
		assertEquals(4, result.positions.get(0).slotEntry.newSumSlot);
		assertEquals(8, result.positions.get(0).slotEntry.slot.slotNum);
		assertTrue(result.reason.isManuel);
		assertEquals("some other reason", result.reason.comment);
	}

	@Test
	void testTransfer() throws ArticleSlotConstraintFailedException {
		Article article = new Article();
		article.warehouseUnitsPerSlot = 20;
		article.id = 1;
		WarehouseBookingPositionSlotEntry sourceSlot = new WarehouseBookingPositionSlotEntry();
		sourceSlot.article = article;
		sourceSlot.slot = new Slot();
		sourceSlot.slot.slotNum = 11;
		sourceSlot.newSumSlot = 17;
		WarehouseBookingPositionSlotEntry destinationSlot = new WarehouseBookingPositionSlotEntry();
		destinationSlot.article = article;
		destinationSlot.slot = new Slot();
		destinationSlot.slot.slotNum = 15;
		destinationSlot.newSumSlot = 2;
		
		WarehouseBooking result = new CreateManuelBookingAction(article, 14, Optional.of(sourceSlot), Optional.of(destinationSlot), "transfer").finish();
		
		assertEquals(-14, result.positions.get(0).amount);
		assertEquals(3, result.positions.get(0).slotEntry.newSumSlot);
		assertEquals(11, result.positions.get(0).slotEntry.slot.slotNum);
		assertEquals(14, result.positions.get(1).amount);
		assertEquals(16, result.positions.get(1).slotEntry.newSumSlot);
		assertEquals(15, result.positions.get(1).slotEntry.slot.slotNum);
		assertTrue(result.reason.isManuel);
		assertEquals("transfer", result.reason.comment);
	}

}
