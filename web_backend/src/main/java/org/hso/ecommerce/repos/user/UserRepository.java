package org.hso.ecommerce.repos.user;

import org.hso.ecommerce.entities.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	@Query("SELECT c FROM User c WHERE c.email = :email")
	Optional<User> findByEmail(String email);

    @Query("SELECT COUNT(c.id) FROM User c WHERE c.created >= :begin AND c.created < :end AND c.isEmployee = false")
    Integer countUsersInTimespan(
            java.sql.Timestamp begin, java.sql.Timestamp end
    );

	@Query("SELECT count(*) FROM User WHERE isEmployee = true")
	Optional<Integer> numberOfEmployees();
}
