package org.hso.ecommerce.supplier.carrier;

import java.time.LocalDateTime;
import java.util.Random;

public class Posaidon implements Carrier {
    @Override
    public String getName() {
        return "Poseidon Inc.";
    }

    @Override
    public String generateTrackingId() {
        Random rnd = new Random();
        return "WAT"
                + Integer.toString(rnd.nextInt(Short.MAX_VALUE))
                + "3"
                + Integer.toString(rnd.nextInt(Short.MAX_VALUE))
                + "R";
    }

    @Override
    public LocalDateTime arrivalEstimate() {
        return LocalDateTime.now().plusHours(50);
    }
}
