package org.hso.ecommerce.controller.intern;

import org.hso.ecommerce.repos.warehouse.WarehouseBookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/intern/warehouse/")
public class WarehouseController {

    @Autowired
    private final WarehouseBookingRepository warehouseBookingRepository = null;

    @GetMapping("/")
    public String accountingWarehouse(
            Model model,
            HttpServletRequest request
    ) {
        model.addAttribute("bookings", warehouseBookingRepository.findAllDesc());
        return "intern/warehouse/index";
    }
}
