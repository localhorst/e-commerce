package org.hso.ecommerce.entities.shop;

import javax.persistence.Embeddable;

@Embeddable
public class Address {
    public String name = "";
    public String addressString = "";
    public String country = "DE";

    @Override
    public String toString() {
        return name + "\n" + addressString;
    }

    public static Address fromString(String addr) {
        Address a = new Address();

        String[] arr = addr.split("\n", 2);
        a.name = arr[0];
        if (arr.length > 1) {
            a.addressString = arr[1];
        }

        return a;
    }
}
