package org.hso.ecommerce.entities.warehouse;

import org.hso.ecommerce.entities.shop.Article;

import javax.persistence.*;

// TODO Unify with $$$ Bookings + WarehouseBookingEntry.

@Entity
@Table(name = "warehouse_booking_positions")
public class WarehouseBookingPosition {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic
    public long id;

    @ManyToOne(optional = false)
    public WarehouseBooking booking;

    @ManyToOne(optional = false)
    public Article article;

    public int amount; // positive or negative

    @ManyToOne(optional = true, cascade = CascadeType.ALL)
    public WarehouseBookingPositionSlotEntry slotEntry;
}
