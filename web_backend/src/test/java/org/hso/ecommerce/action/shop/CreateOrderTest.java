package org.hso.ecommerce.action.shop;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.hso.ecommerce.action.shop.CreateOrderAction.ArticleNotInStockException;
import org.hso.ecommerce.action.shop.CreateOrderAction.Result;
import org.hso.ecommerce.entities.booking.BookingAccountEntry;
import org.hso.ecommerce.entities.booking.PaymentMethod;
import org.hso.ecommerce.entities.shop.Address;
import org.hso.ecommerce.entities.shop.Article;
import org.hso.ecommerce.entities.supplier.ArticleOffer;
import org.hso.ecommerce.entities.user.User;
import org.hso.ecommerce.entities.warehouse.Slot;
import org.hso.ecommerce.entities.warehouse.WarehouseBookingPosition;
import org.hso.ecommerce.entities.warehouse.WarehouseBookingPositionSlotEntry;
import org.junit.jupiter.api.Test;

class CreateOrderTest {

	@Test
	void test() throws ArticleNotInStockException {
		User user = new User();
		int expectedTotalGrossCent = 28_83;
		Address destination = new Address();
		PaymentMethod method = new PaymentMethod();

		BookingAccountEntry latestUserBooking = new BookingAccountEntry();
		latestUserBooking.newSumCent = 0;
		latestUserBooking.userAccount = user;

		BookingAccountEntry latestVatBooking = new BookingAccountEntry();
		latestVatBooking.isVATAccount = true;
		latestVatBooking.newSumCent = 100_00;

		BookingAccountEntry latestMainBooking = new BookingAccountEntry();
		latestMainBooking.isMainAccount = true;
		latestMainBooking.newSumCent = 500_00;

		CreateOrderAction action = new CreateOrderAction(user, expectedTotalGrossCent, destination, method,
				latestUserBooking, latestVatBooking, latestMainBooking);

		// orders
		ArticleOffer fantaOffer = new ArticleOffer();
		fantaOffer.vatPercent = 7;
		Article fanta = new Article();
		fanta.shopPricePerUnitNetCent = 1_40;
		fanta.related = fantaOffer;
		fanta.warehouseUnitsPerSlot = 20;
		action.addArticle(fanta, 10, slots(fanta, 1, new int[] { 20, 3 }));

		ArticleOffer colaOffer = new ArticleOffer();
		colaOffer.vatPercent = 7;
		Article cola = new Article();
		cola.shopPricePerUnitNetCent = 1_86;
		cola.related = colaOffer;
		cola.warehouseUnitsPerSlot = 20;
		action.addArticle(cola, 7, slots(cola, 4, new int[] { 2, 20, 18, 3 }));

		Result result = action.finish();

		assertEquals(28_83, result.bookings.get(0).amountCent); // purchase
		assertTrue(result.bookings.get(0).destination.isMainAccount);
		assertEquals(28_83, result.bookings.get(1).amountCent); // payment
		assertEquals(user, result.bookings.get(1).destination.userAccount);
		assertEquals(1_81, result.bookings.get(2).amountCent); // vat
		assertTrue(result.bookings.get(2).destination.isVATAccount);

		assertEquals(2, result.customerOrder.positions.size());
		assertEquals(28_83, result.customerOrder.totalGrossCent);
		assertEquals(27_02, result.customerOrder.totalNetCent);

		List<WarehouseBookingPosition> warehousePositions = result.warehouseBooking.positions;
		assertEquals(-7, find(warehousePositions, 1).amount);
		assertEquals(-3, find(warehousePositions, 2).amount);
		assertEquals(-2, find(warehousePositions, 4).amount);
		assertEquals(-2, find(warehousePositions, 6).amount);
		assertEquals(-3, find(warehousePositions, 7).amount);
		// Check the remaining amount of one slot as an example
		assertEquals(16, find(warehousePositions, 6).slotEntry.newSumSlot);
	}

	private List<WarehouseBookingPositionSlotEntry> slots(Article article, int firstSlot, int[] amounts) {
		List<WarehouseBookingPositionSlotEntry> entries = new ArrayList<>();
		for (int i = 0; i < amounts.length; i++) {
			WarehouseBookingPositionSlotEntry entry = new WarehouseBookingPositionSlotEntry();
			entry.article = article;
			entry.newSumSlot = amounts[i];
			entry.slot = new Slot();
			entry.slot.slotNum = firstSlot + i;
			entries.add(entry);
		}
		return entries;
	}

	private WarehouseBookingPosition find(List<WarehouseBookingPosition> positions, int slotNumber) {
		for (WarehouseBookingPosition position : positions) {
			if (position.slotEntry.slot.slotNum == slotNumber) {
				return position;
			}
		}
		return null;
	}

}
