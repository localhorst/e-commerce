package org.hso.ecommerce.repos.shop;

import org.hso.ecommerce.entities.shop.CustomerOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerOrderRepository extends JpaRepository<CustomerOrder, Long> {

    @Query("SELECT SUM(cop.quantity) FROM CustomerOrderPosition cop JOIN cop.order co WHERE cop.article.id = :articleId AND co.created >= :begin AND co.created < :end")
    Integer countOrdersOfArticleInTimespan(
            long articleId, java.sql.Timestamp begin, java.sql.Timestamp end
    );

    @Query("SELECT co FROM CustomerOrder co ORDER BY co.id DESC")
    List<CustomerOrder> getAllOrders();

    @Query("SELECT co FROM CustomerOrder co WHERE co.customer.id = :userId ORDER BY co.id DESC")
    List<CustomerOrder> getOrdersByUserId(long userId);

    @Query("SELECT COUNT(co.id) FROM CustomerOrder co WHERE co.created >= :begin AND co.created < :end")
    Integer countOrdersInTimespan(
            java.sql.Timestamp begin, java.sql.Timestamp end
    );

    @Query("SELECT SUM(co.totalGrossCent) FROM CustomerOrder co WHERE co.created >= :begin AND co.created < :end")
    Integer countTurnoverInTimespan(
            java.sql.Timestamp begin, java.sql.Timestamp end
    );

}