package org.hso.ecommerce.supplier.data;

import java.time.LocalDateTime;

public class OrderConfirmation {
    public String manufacturer;
    public String articleNumber;

    public int quantity;

    public int pricePerUnitNetCent;
    public int discountNetCent;
    public int totalPriceNetCharged;

    public String carrier;
    public String trackingId;
    public LocalDateTime estimatedArrival;
}
