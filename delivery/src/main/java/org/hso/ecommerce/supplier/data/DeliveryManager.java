package org.hso.ecommerce.supplier.data;

import java.util.HashMap;

public class DeliveryManager {

    private HashMap<String, Delivery> deliveryList;
    private static DeliveryManager deliveryManager;

    private DeliveryManager()
    {
        deliveryList = new HashMap<>();
    }

    public static DeliveryManager getInstance() {

        if (DeliveryManager.deliveryManager == null) {
            DeliveryManager.deliveryManager = new DeliveryManager();
        }
        return DeliveryManager.deliveryManager;
    }

    public void add(Delivery delivery) {
        deliveryList.put(delivery.getUuid(), delivery);
    }

    public Delivery getDeliveryByID(String uuid) {
        return deliveryList.getOrDefault(uuid, null);
    }
}
