package org.hso.ecommerce.entities.booking;

import javax.persistence.Embeddable;

@Embeddable
public class PaymentMethod {

    public String creditCardNumber;

    public static PaymentMethod fromCreditCardNumber(String cardnumber) {
        PaymentMethod m = new PaymentMethod();
        m.creditCardNumber = cardnumber;

        return m;
    }
}
