document.addEventListener("DOMContentLoaded", function(event) {
   let back = document.getElementsByClassName('back')[0];
   if (!back) {
      return;
   }

   let group = back.dataset.group || 'default';
   let name = back.dataset.name;
   let insert = back.dataset.insert == "true";

   if (name) {
      window.sessionStorage.setItem('back-' + group, JSON.stringify({
         'text': name,
         'location': window.location.toString()
      }));
   }

   let history = JSON.parse(window.sessionStorage.getItem('back-' + group));
   if (insert && history) {

      let a = document.createElement("a");
      a.href = "#";
      a.onclick = function() {
         window.location = history.location;
      };

      a.innerText = history.text || 'Zurück';
      back.appendChild(a);
   }

});
