package org.hso.ecommerce.action.shop;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.hso.ecommerce.entities.shop.Article;
import org.junit.jupiter.api.Test;

class GetRandomArticlesTest {

	@Test
	void test() {
		List<Article> articles = new ArrayList<>();
		for (int i = 0; i < 10 ; i++) {
			Article article = new Article();
			article.id = i;
			articles.add(article);
		}

		int[] counters = new int[10];
		// Choose 10000 times 3 articles out of 10
		for (int i = 0; i < 10_000; i++) {
			List<Article> chosen = GetRandomArticlesAction.getRandomArticles(3, new ArrayList<>(articles));
			assertEquals(3, chosen.size());
			for (Article chosenArticle : chosen) {
				counters[(int) chosenArticle.id]++;
			}
		}

		// Expect each article to be chosen 2400 - 3600 times.
		// The probability for this test to fail randomly is below 10^-36
		for (int counterValue : counters) {
			assertTrue(counterValue >= 2400);
			assertTrue(counterValue <= 3600);
		}
	}

}
