package org.hso.ecommerce.entities.booking;

import org.hso.ecommerce.entities.shop.CustomerOrder;
import org.hso.ecommerce.entities.supplier.Supplier;
import org.hso.ecommerce.entities.supplier.SupplierOrder;

import javax.persistence.*;

@Entity
@Table(name = "booking_reasons")
public class BookingReason {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic
    public long id;

    public boolean isManuel;
    public boolean isStartBooking;

    public String comment;

    @ManyToOne(optional = true)
    public CustomerOrder customerOrder;

    @OneToOne(optional = true, cascade = CascadeType.ALL)
    public CustomerPayment customerPayment;

    @ManyToOne(optional = true)
    public SupplierOrder supplierOrder;

    @ManyToOne(optional = true)
    public Supplier supplierPayment;

    // Default Constructor is needed for construction by ORM
    public BookingReason() {
    }

    public BookingReason(CustomerOrder order) {
        this.customerOrder = order;
    }

    public BookingReason(CustomerPayment customerPayment) {
        this.customerPayment = customerPayment;
    }

    public BookingReason(Supplier supplierPayment) {
        this.supplierPayment = supplierPayment;
    }

    public BookingReason(SupplierOrder supplierOrder) {
        this.supplierOrder = supplierOrder;
    }

    public BookingReason(String comment) {
        this.isManuel = true;
        this.comment = comment;
    }
}
