/*
* add a supplier first
*/

INSERT INTO article_offers ("manufacturer", "article_number", "price_per_unit_net", "title", "vat_percent", "should_be_advertised", "cheapest_supplier_id")
VALUES ("McDonalds", "1", 4242, "McPizza", 7, 1, 1);



/*
* There is no need for the add article, you can add one form the UI on the offerd article page
*/
INSERT INTO articles ("related_id", "shop_price_per_unit_net_cent", "warehouse_units_per_slot", "should_reorder", "reorder_max_price", "title", "description", "image_id")
VALUES (1, 19.99, 10, 1, 15, "Huge Hamburger", "This huge Hamburger is awesome!", NULL);

