package org.hso.ecommerce.app;

import org.hso.ecommerce.components.ErrorDemoInterceptor;
import org.hso.ecommerce.components.InfoDemoInterceptor;
import org.hso.ecommerce.components.LoginIntercepter;
import org.hso.ecommerce.components.ShoppingCartInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class Config implements WebMvcConfigurer {

    // Copied from https://stackoverflow.com/questions/18218386/cannot-autowire-service-in-handlerinterceptoradapter/18218439
    // Currently not needed for the other interceptors. They will be removed anyway.
    @Bean
    public LoginIntercepter buildLoginIntercepter() {
        return new LoginIntercepter();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(buildLoginIntercepter());
        registry.addInterceptor(new ErrorDemoInterceptor());
        registry.addInterceptor(new InfoDemoInterceptor());
        registry.addInterceptor(new ShoppingCartInterceptor());

    }
}
