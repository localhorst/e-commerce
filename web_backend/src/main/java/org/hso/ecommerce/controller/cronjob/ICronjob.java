package org.hso.ecommerce.controller.cronjob;


import java.util.Calendar;

interface ICronjob {
    /**
     * Calculate the earliest cronjob execution time that happens after the given reference time.
     *
     * @param reference Position in time to start searching. The implementor is allowed to modify the reference time.
     * @return A new Calendar instance (or the same) containing the time for next execution.
     */
    Calendar nextExecution(Calendar reference);

    /**
     * Calculate the latest cronjob execution time that happens before or exactly at the given refernce time.
     *
     * @param reference Position in time to start searching. The implementor is allowed to modify the reference time.
     * @return A new Calendar instance (or the same) containing the time of the last execution.
     */
    Calendar previousExecution(Calendar reference);

    /**
     * Execute this cronjob.
     *
     * @param time       The point in time this execution was scheduled. In case of a missed cronjob, the actual time of
     *                   this call might be much later.
     * @param controller Back-reference that allows to use repositories.
     */
    void executeAt(Calendar time, CronjobController controller);

    /**
     * Get a name for this cronjob, that can be presented to the user in the frontend.
     *
     * @return A german name of this cronjob.
     */
    String getDisplayName();
}