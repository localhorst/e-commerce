package org.hso.ecommerce.entities.shop;

import javax.persistence.*;

@Entity
@Table(name = "images")
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic
    public long id;

    public String path;
}