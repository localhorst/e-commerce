package org.hso.ecommerce.action.booking;

import static org.junit.jupiter.api.Assertions.*;

import org.hso.ecommerce.entities.booking.Booking;
import org.hso.ecommerce.entities.booking.BookingAccountEntry;
import org.hso.ecommerce.entities.booking.BookingReason;
import org.junit.jupiter.api.Test;

class CreateBookingTest {

	@Test
	void test() {
		BookingAccountEntry sourceAccount = new BookingAccountEntry();
		sourceAccount.isMainAccount = true;
		sourceAccount.newSumCent = 600_00;
		BookingAccountEntry destinationAccount = new BookingAccountEntry();
		destinationAccount.isVATAccount = true;
		destinationAccount.newSumCent = 100_00;
		BookingReason reason = new BookingReason();
		reason.comment = "some test";

		Booking result = new CreateBookingAction(sourceAccount, destinationAccount, reason, 42_00).finish();

		assertTrue(result.source.isMainAccount);
		assertFalse(result.source.isVATAccount);
		assertEquals(558_00, result.source.newSumCent);

		assertFalse(result.destination.isMainAccount);
		assertTrue(result.destination.isVATAccount);
		assertEquals(142_00, result.destination.newSumCent);

		assertEquals("some test", result.reason.comment);
		assertEquals(42_00, result.amountCent);
	}

}
