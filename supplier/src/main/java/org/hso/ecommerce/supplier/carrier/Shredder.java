package org.hso.ecommerce.supplier.carrier;

import java.time.LocalDateTime;
import java.util.Random;

public class Shredder implements Carrier {

    private Random rnd = new Random();

    @Override
    public String getName() {
        return "Schree & Derr";
    }

    @Override
    public String generateTrackingId() {
        return "O" + d() + d() + d() + d() + d() + d() + d() + d() + d() + d() + d() + d() + d() + d() + d() + d() + d() + d() + "0";
    }

    @Override
    public LocalDateTime arrivalEstimate() {
        return LocalDateTime.now().plusHours(22);
    }

    /**
     * @return a random digit followed by a dash.
     */
    private String d() {
        return Integer.toString(rnd.nextInt(9)) + "-";
    }
}
