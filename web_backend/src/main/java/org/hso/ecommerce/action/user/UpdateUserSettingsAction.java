package org.hso.ecommerce.action.user;

import org.hso.ecommerce.entities.booking.PaymentMethod;
import org.hso.ecommerce.entities.user.User;
import org.hso.ecommerce.repos.user.UserRepository;

public class UpdateUserSettingsAction {

    private User user;
    private UserRepository repository;

    public UpdateUserSettingsAction(User user, UserRepository repository) {
        this.user = user;
        this.repository = repository;
    }

    public UpdateResult updateEmail(String newMail) {
        UpdateResult result = new UpdateResult(false);
        if (!newMail.contains("@")) {
            result.errorString = "Ändern der Email-Addresse nicht möglich. Bitte versuchen Sie es erneut.";
        } else {
            this.user.email = newMail;
            this.repository.save(this.user);
            result.updated = true;
        }
        return result;
    }

    public UpdateResult updatePassword(String oldPassword, String password1, String password2) {
        UpdateResult result = new UpdateResult(false);
        if (this.user.validatePassword(oldPassword)) {
            if (password1.equals(password2)) {
                if (!password1.equals(oldPassword)) {
                    this.user.setPassword(password1);
                    this.repository.save(this.user);
                    result.updated = true;
                } else {
                    result.errorString = "Das neue Passwort entspricht dem alten Passwort.";
                }
            } else {
                result.errorString = "Die beiden neuen Passwörter stimmen nicht überein. Bitte versuchen Sie es erneut.";
            }
        } else {
            result.errorString = "Das eingegebene alte Passwort stimmt nicht mit dem momentan gespeicherten Passwort überein. Bitte versuchen Sie es erneut.";
        }
        return result;
    }

    public UpdateResult updateShippingInfo(String salutation, String name, String address) {
        this.user.salutation = salutation;
        this.user.defaultDeliveryAddress.name = name;
        this.user.defaultDeliveryAddress.addressString = address;
        this.repository.save(this.user);
        return new UpdateResult(true);
    }

    public UpdateResult updatePaymentInfo(String creditCardNumber) {
        UpdateResult result = new UpdateResult(false);
        if (creditCardNumber.matches("[0-9]+")) {
            this.user.defaultPayment = PaymentMethod.fromCreditCardNumber(creditCardNumber);
            this.repository.save(this.user);
            result.updated = true;
        } else {
            result.errorString = "Kreditkartennummer darf nur Zahlen enthalten. Bitte versuchen Sie es erneut.";
        }
        return result;
    }

    public class UpdateResult {
        public boolean updated;  //if true worked, if false not worked
        public String errorString;

        public UpdateResult(boolean updated, String errorString) {
            this.updated = updated;
            this.errorString = errorString;
        }

        public UpdateResult(boolean updated) {
            this.updated = updated;
            this.errorString = "";
        }
    }
}
