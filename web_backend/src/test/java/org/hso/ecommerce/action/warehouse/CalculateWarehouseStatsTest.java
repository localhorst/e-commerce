package org.hso.ecommerce.action.warehouse;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.hso.ecommerce.entities.shop.Article;
import org.hso.ecommerce.entities.warehouse.Slot;
import org.hso.ecommerce.entities.warehouse.WarehouseBookingPositionSlotEntry;
import org.junit.jupiter.api.Test;

class CalculateWarehouseStatsTest {

	@Test
	void test() {
		// articles with different amounts of items that fit in one warehouse slot
		Article fanta = new Article();
		fanta.id = 1;
		fanta.warehouseUnitsPerSlot = 18;
		
		Article cola = new Article();
		cola.id = 2;
		cola.warehouseUnitsPerSlot = 7;

		List<WarehouseBookingPositionSlotEntry> entries = new ArrayList<>();
		for (int i=1; i<=20; i++) {
			WarehouseBookingPositionSlotEntry entry = new WarehouseBookingPositionSlotEntry();
			entry.slot = new Slot();
			entry.slot.slotNum = i;
			entries.add(entry);
		}
		entries.get(0).article = fanta;
		entries.get(0).newSumSlot = 12;
		entries.get(3).article = cola;
		entries.get(3).newSumSlot = 7;
		entries.get(4).article = fanta;
		entries.get(4).newSumSlot = 18;
		entries.get(11).article = fanta;
		entries.get(11).newSumSlot = 2;
		entries.get(15).article = cola;
		entries.get(15).newSumSlot = 5;

		CalculateWarehouseStatsAction.WarehouseStats result = new CalculateWarehouseStatsAction(entries).finish();

		assertEquals(2, result.numArticles);
		assertSimilar(5.0 / 20.0, result.ratioUsedSlots);
		assertSimilar(32.0 / 360.0 + 12.0 / 140.0, result.efficiency);
	}
	
	private void assertSimilar(double expected, double actual) {
		double diff = actual - expected;
		assertTrue(diff < 1e-5 && diff > -1e-5);
	}

}
