package org.hso.ecommerce.app.config;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import org.hso.ecommerce.app.config.YAMLData.Address;
import org.springframework.stereotype.Component;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import javax.annotation.PostConstruct;

@Component("appSettings")
public class AppSettings {

	private YAMLData data;
	private final String configFile = "config.yml";

	private String installationName;
	private String companyName;
	private Address companyAddress;
	private int numberOfStorageSpaces;
	private List<YAMLData.Supplier> suppliers;
	private String parcelServiceName;
	private String parcelServiceApiURL;
	
	/**
	 * on initialization read the config and store the data in static objects
	 */
	@PostConstruct
	public void init() {
		data = readConfig();

		installationName = data.getInstallationName();
		companyName = data.getCompanyName();
		companyAddress = data.getCompanyAddress();
		numberOfStorageSpaces = data.getNumberOfStorageSpaces();
		suppliers = data.getSuppliers();
		parcelServiceName = data.getParcelServiceName();
		parcelServiceApiURL = data.getParcelServiceApiURL();

		System.out.println("Initialised Settings!");
	}

	/**
	 * write the default config file
	 */
	public void writeDefaultConfig() {
		YAMLData data = new YAMLData();
		data.setInstallationName("eCommerce");
		data.setCompanyName("eCommerce Shop UG");
		data.setCompanyAddress(new Address(
				"Musterstraße",
				"1",
				"12345",
				"Musterstadt",
				"Germany"
		));
		data.setNumberOfStorageSpaces(128);

		List<YAMLData.Supplier> suppliers = new ArrayList<>();
		suppliers.add(new YAMLData.Supplier(
				"Bank of Chees",
				"d41d8cd98f00b204e9800998ecf8427e",
				"http://[::1]:8081/bank/",
				4,
				new Address(
						"Musterstraße",
						"2",
						"12345",
						"Musterstadt",
						"Germany"
				)
		));
		suppliers.add(new YAMLData.Supplier(
				"MDA",
				"18a17da5bac1cf00551b08c3e98720f5",
				"http://[::1]:8081/mda/",
				5,
				new Address(
						"Musterstraße",
						"3",
						"12345",
						"Musterstadt",
						"Germany"
				)
		));
		data.setSuppliers(suppliers);

		data.setParcelServiceName("Parcel Service");
		data.setParcelServiceApiURL("http://[::1]:8082/");

		try (FileWriter writer = new FileWriter("./" + configFile)) {
			Yaml yaml = new Yaml();
			yaml.dump(data, writer);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * read a config file named "eCommerce_config.yml" from the applications root directory
	 * @return the settings as YAMLData object
	 */
	public YAMLData readConfig() {
		YAMLData data = new YAMLData();

		File file = new File("./" + configFile);
		if (!file.exists()) {
			writeDefaultConfig();
		}

		try (InputStream inputStream = new FileInputStream("./" + configFile)) {
			Yaml yaml = new Yaml(new Constructor(YAMLData.class));
			data = yaml.load(inputStream);
		} catch (FileNotFoundException e) {
			System.err.println("The file \"" + configFile + "\" has not been found, please create a valid Configuration file.");
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		return data;
	}

	public YAMLData getData() {
		return data;
	}

	public String getInstallationName() {
		return installationName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public Address getCompanyAddress() {
		return companyAddress;
	}

	public int getNumberOfStorageSpaces() {
		return numberOfStorageSpaces;
	}

	public List<YAMLData.Supplier> getSuppliers() {
		return suppliers;
	}

	public String getParcelServiceName() {
		return parcelServiceName;
	}

	public String getParcelServiceApiURL() {
		return parcelServiceApiURL;
	}
}