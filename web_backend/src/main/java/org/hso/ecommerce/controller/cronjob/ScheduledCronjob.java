package org.hso.ecommerce.controller.cronjob;

import org.hso.ecommerce.entities.cron.BackgroundJob;

import java.util.Calendar;

class ScheduledCronjob {
    public final Calendar executionTime;
    public final ICronjob cronjob;
    public final BackgroundJob model;

    public ScheduledCronjob(Calendar executionTime, ICronjob cronjob, BackgroundJob model) {
        this.executionTime = executionTime;
        this.cronjob = cronjob;
        this.model = model;
    }
}