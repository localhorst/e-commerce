package org.hso.ecommerce.entities.shop;

import org.hso.ecommerce.entities.supplier.ArticleOffer;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "articles")
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic
    public long id;

    @OneToOne(optional = false)
    public ArticleOffer related;

    public int shopPricePerUnitNetCent;
    public int warehouseUnitsPerSlot;

    public boolean shouldReorder;
    public int reorderMaxPrice;

    @NotNull
    public String title;

    @NotNull
    public String description;

    @OneToOne(optional = true)
    @Basic(fetch = FetchType.LAZY)
    public Image image;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "article_categories_bindings")
    public Set<Category> categories = new HashSet<>();

    public int getVat() {
        return (shopPricePerUnitNetCent * related.vatPercent) / 100;
    }

    public int getPriceGross() {
        return shopPricePerUnitNetCent + getVat();
    }
}