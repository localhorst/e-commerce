package org.hso.ecommerce.action.cronjob;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.hso.ecommerce.action.cronjob.ReadSupplierDataAction.ArticleIdentifier;
import org.hso.ecommerce.action.cronjob.ReadSupplierDataAction.Offer;
import org.hso.ecommerce.entities.supplier.ArticleOffer;
import org.hso.ecommerce.entities.supplier.Supplier;
import org.junit.jupiter.api.Test;

class UpdateOffersTest {

	@Test
	void test() {
		// suppliers
		Supplier mcDonalds = new Supplier();
		Supplier burgerKing = new Supplier();
		org.hso.ecommerce.api.data.Supplier apiMcDonalds = new org.hso.ecommerce.api.data.Supplier();
		apiMcDonalds.articles = new ArrayList<>();
		org.hso.ecommerce.api.data.Supplier apiBurgerKing = new org.hso.ecommerce.api.data.Supplier();
		apiBurgerKing.articles = new ArrayList<>();

		// previously stored offers
		List<ArticleOffer> offers = new ArrayList<>();
		ArticleOffer fries = new ArticleOffer();
		fries.manufacturer = "potatoe Inc.";
		fries.articleNumber = "FR-13S";
		fries.cheapestSupplier = mcDonalds;
		offers.add(fries);

		ArticleOffer burger = new ArticleOffer();
		burger.manufacturer = "Hamburger GmbH";
		burger.articleNumber = "BU-4934";
		burger.cheapestSupplier = mcDonalds;
		offers.add(burger);

		ArticleOffer pizza = new ArticleOffer();
		pizza.manufacturer = "Teig Verarbeitung AG";
		pizza.articleNumber = "3.14-ZZA";
		offers.add(pizza);

		ArticleOffer donut = new ArticleOffer();
		donut.manufacturer = "Hevert Süßwarengroßhändler";
		donut.articleNumber = "D-HITM";
		donut.shouldBeAdvertised = true;
		offers.add(donut);
		
		// new offers
		org.hso.ecommerce.api.data.Article apiPizza = new org.hso.ecommerce.api.data.Article();
		apiPizza.title = "Pizza";
		apiPizza.manufacturer = "Teig Verarbeitung AG";
		apiPizza.articleNumber = "3.14-ZZA";
		apiPizza.shouldBeAdvertised = true;
		apiMcDonalds.articles.add(apiPizza);
		
		org.hso.ecommerce.api.data.Article apiSausage = new org.hso.ecommerce.api.data.Article();
		apiSausage.title = "Wurst";
		apiSausage.manufacturer = "Schlachterei Müller";
		apiSausage.articleNumber = "SGE-24";
		apiBurgerKing.articles.add(apiSausage);
		
		org.hso.ecommerce.api.data.Article apiBurger = new org.hso.ecommerce.api.data.Article();
		apiBurger.title = "Burger";
		apiBurger.manufacturer = "Hamburger GmbH";
		apiBurger.articleNumber = "BU-4934";
		apiBurgerKing.articles.add(apiBurger);
		
		org.hso.ecommerce.api.data.Article apiDonut = new org.hso.ecommerce.api.data.Article();
		apiDonut.title = "Donut";
		apiDonut.manufacturer = "Hevert Süßwarengroßhändler";
		apiDonut.articleNumber = "D-HITM";
		apiBurgerKing.articles.add(apiDonut);

		// The currently cheapest suppliers
		HashMap<ArticleIdentifier, Offer> cheapestOffers = new HashMap<>();
		cheapestOffers.put(new ArticleIdentifier("Schlachterei Müller", "SGE-24"),
				new Offer(burgerKing, apiBurgerKing));
		cheapestOffers.put(new ArticleIdentifier("Hamburger GmbH", "BU-4934"), new Offer(burgerKing, apiBurgerKing));
		cheapestOffers.put(new ArticleIdentifier("Teig Verarbeitung AG", "3.14-ZZA"),
				new Offer(mcDonalds, apiMcDonalds));
		cheapestOffers.put(new ArticleIdentifier("Hevert Süßwarengroßhändler", "D-HITM"),
				new Offer(burgerKing, apiBurgerKing));

		List<ArticleOffer> result = new UpdateOffersAction(offers, cheapestOffers).finish();

		// fries are no longer available
		assertEquals(null, find(result, "potatoe Inc.", "FR-13S").cheapestSupplier);
		
		// the sausage was not available before, but is now
		assertEquals("Wurst", find(result, "Schlachterei Müller", "SGE-24").title);
		
		// BurgerKing offers burgers cheaper than McDonalds
		assertEquals(burgerKing, find(result, "Hamburger GmbH", "BU-4934").cheapestSupplier);
		
		// pizza should now be advertised
		assertTrue(find(result, "Teig Verarbeitung AG", "3.14-ZZA").shouldBeAdvertised);
		
		// donuts should no longer be advertised
		assertFalse(find(result, "Hevert Süßwarengroßhändler", "D-HITM").shouldBeAdvertised);
	}

	private ArticleOffer find(List<ArticleOffer> list, String manufacturer, String articleNumber) {
		for (ArticleOffer offer : list) {
			if (offer.manufacturer.equals(manufacturer) && offer.articleNumber.equals(articleNumber)) {
				return offer;
			}
		}
		return null;
	}

}
