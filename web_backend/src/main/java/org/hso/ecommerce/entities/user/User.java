package org.hso.ecommerce.entities.user;

import org.hso.ecommerce.entities.booking.PaymentMethod;
import org.hso.ecommerce.entities.shop.Address;
import org.springframework.security.crypto.bcrypt.BCrypt;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic
    public long id;

    @NotNull
    public java.sql.Timestamp created;

    @NotNull
    @Column(unique = true)
    public String email;

    public String salutation;

    public String passwordHash;

    public boolean isActive;
    public boolean isEmployee;

    @Embedded
    public Address defaultDeliveryAddress;

    @Embedded
    public PaymentMethod defaultPayment;

    public long getId() {
        return id;
    }

    public boolean validatePassword(String password) {
        return BCrypt.checkpw(password, passwordHash);
    }

    public void setPassword(String password) {
        passwordHash = BCrypt.hashpw(password, BCrypt.gensalt());
    }
}
