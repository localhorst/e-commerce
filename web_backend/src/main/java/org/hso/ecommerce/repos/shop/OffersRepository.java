package org.hso.ecommerce.repos.shop;

import java.util.List;

import org.hso.ecommerce.entities.supplier.ArticleOffer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OffersRepository extends JpaRepository<ArticleOffer, Long> {

	@Query("SELECT a FROM ArticleOffer a")
	List<ArticleOffer> findAll();
	
	@Query("SELECT a FROM ArticleOffer a WHERE a.id = :offeredarticleId")
	ArticleOffer findOfferedArticleById(@Param("offeredarticleId") long offeredarticleId);

}
