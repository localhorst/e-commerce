package org.hso.ecommerce.api.data;

public class SupplierDiscount {
    public int minimumDailySalesVolumeNetCent;
    public int percentDiscount;
}
