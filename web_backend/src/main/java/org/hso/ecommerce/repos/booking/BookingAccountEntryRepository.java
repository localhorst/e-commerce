package org.hso.ecommerce.repos.booking;

import java.util.Optional;

import org.hso.ecommerce.entities.booking.BookingAccountEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface BookingAccountEntryRepository extends JpaRepository<BookingAccountEntry, Long> {

    @Query(value = "SELECT * FROM booking_account_entries as e WHERE e.user_account_id = :user ORDER BY e.id DESC LIMIT 1", nativeQuery = true)
    Optional<BookingAccountEntry> getByUser(Long user);

    @Query(value = "SELECT * FROM booking_account_entries as e WHERE e.supplier_account_id = :supplier ORDER BY e.id DESC LIMIT 1", nativeQuery = true)
    Optional<BookingAccountEntry> getBySupplier(Long supplier);

    @Query(value = "SELECT * FROM booking_account_entries as e WHERE e.is_main_account = 1 ORDER BY e.id DESC LIMIT 1", nativeQuery = true)
    Optional<BookingAccountEntry> getByMain();

    @Query(value = "SELECT * FROM booking_account_entries as e WHERE e.isvataccount = 1 ORDER BY e.id DESC LIMIT 1", nativeQuery = true)
    Optional<BookingAccountEntry> getByVat();


}


