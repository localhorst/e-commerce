package org.hso.ecommerce.entities.booking;

import org.hso.ecommerce.entities.booking.PaymentMethod;

import javax.persistence.*;

@Entity
@Table(name = "customer_payments")
public class CustomerPayment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic
    public long id;

    public int amountCent;

    @Embedded
    public PaymentMethod payment;
}
