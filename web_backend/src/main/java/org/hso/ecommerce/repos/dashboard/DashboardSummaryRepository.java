package org.hso.ecommerce.repos.dashboard;

import org.hso.ecommerce.entities.dashboard.DashboardSummary;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface DashboardSummaryRepository  extends JpaRepository<DashboardSummary, Long> {

    @Query("SELECT ds FROM DashboardSummary ds ORDER BY ds.id DESC")
    List<DashboardSummary> findInTimespan(
            Pageable pageable
    );

}
