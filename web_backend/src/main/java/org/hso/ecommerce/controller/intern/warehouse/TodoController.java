package org.hso.ecommerce.controller.intern.warehouse;

import org.hso.ecommerce.action.shop.EnableTrackingAction;
import org.hso.ecommerce.api.RestServiceForDelivery;
import org.hso.ecommerce.entities.warehouse.WarehouseBooking;
import org.hso.ecommerce.repos.warehouse.WarehouseBookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.ResourceAccessException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@Controller
@RequestMapping("/intern/warehouse/")
public class TodoController {

    @Autowired
    private final WarehouseBookingRepository warehouseBookingRepository = null;

    @Autowired
    private final RestServiceForDelivery deliveryService = null;

    @GetMapping("todo")
    public String accountingWarehouseTodo(
            Model model
    ) {
        model.addAttribute("bookings", warehouseBookingRepository.findNotDone());
        return "intern/warehouse/todo";
    }


    @PostMapping("progress/{id}")
    public String postProgressId(
            Model model,
            HttpServletRequest request,
            HttpServletResponse response,
            @PathVariable("id") Long id
    ) {
        Optional<WarehouseBooking> booking = warehouseBookingRepository.findById(id);
        if (!booking.isPresent()) {
            model.addAttribute("error", "Die Buchung wurde nicht gefunden.");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return "error/404";
        }

        if (booking.get().isInProgress) {
            response.setStatus(409);
            return "intern/warehouse/error_progress_failed";
        }

        booking.get().isInProgress = true;
        warehouseBookingRepository.save(booking.get());

        return "redirect:/intern/warehouse/progress/" + id;
    }

    @PostMapping("progress/{id}/finish")
    public String postProgressIdFinish(
            Model model,
            HttpServletRequest request,
            HttpServletResponse response,
            @PathVariable("id") Long id
    ) {
        Optional<WarehouseBooking> booking = warehouseBookingRepository.findById(id);
        if (!booking.isPresent()) {
            model.addAttribute("error", "Die Buchung wurde nicht gefunden.");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return "error/404";
        }

        booking.get().isInProgress = true;
        booking.get().isDone = true;

        // Update Delivery Date
        if (booking.get().reason.customerOrder != null) {
            try{
                EnableTrackingAction.addTrackingInfo(deliveryService, booking.get().reason.customerOrder);
            }
            catch(ResourceAccessException e)
            {
                return "error/500";
            }

        }

        warehouseBookingRepository.save(booking.get());

        return "redirect:/intern/warehouse/todo";
    }

    @GetMapping("progress/{id}")
    public String getProgressId(Model model,
                                HttpServletRequest request,
                                HttpServletResponse response,
                                @PathVariable("id") Long id) {
        Optional<WarehouseBooking> booking = warehouseBookingRepository.findById(id);
        if (!booking.isPresent()) {
            model.addAttribute("error", "Die Buchung wurde nicht gefunden.");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return "error/404";
        }

        if (booking.get().isDone) {
            model.addAttribute("info", "Die Buchung wurde schon abgeschlossen.");
        }

        if (!booking.get().isInProgress) {
            // Only reachable if path is manipulated.
            model.addAttribute("error", "Die Buchung wurde noch nicht zugewiesen!");
        }

        model.addAttribute("booking", booking.get());
        return "intern/warehouse/id_progress";
    }


}
