package org.hso.ecommerce.app.config;

import java.util.List;

public class YAMLData {

	private String installationName;
	private String companyName;
	private Address companyAddress;
	private int numberOfStorageSpaces;
	private List<Supplier> suppliers;
	private String parcelServiceName;
	private String parcelServiceApiURL;
	
	
	public String getInstallationName() {
		return installationName;
	}
	public void setInstallationName(String installationName) {
		this.installationName = installationName;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Address getCompanyAddress() {
		return companyAddress;
	}
	public void setCompanyAddress(Address companyAddress) {
		this.companyAddress = companyAddress;
	}
	public int getNumberOfStorageSpaces() {
		return numberOfStorageSpaces;
	}
	public void setNumberOfStorageSpaces(int numberOfStorageSpaces) {
		this.numberOfStorageSpaces = numberOfStorageSpaces;
	}
	public List<Supplier> getSuppliers() {
		return suppliers;
	}
	public void setSuppliers(List<Supplier> suppliers) {
		this.suppliers = suppliers;
	}
	public String getParcelServiceName() {
		return parcelServiceName;
	}
	public void setParcelServiceName(String parcelServiceName) {
		this.parcelServiceName = parcelServiceName;
	}
	public String getParcelServiceApiURL() {
		return parcelServiceApiURL;
	}
	public void setParcelServiceApiURL(String parcelServiceApiURL) {
		this.parcelServiceApiURL = parcelServiceApiURL;
	}
	
	public static class Address {

		public String streetName;
		public String houseNumber;
		public String zipCode;
		public String cityName;
		public String countryName;

		public Address() {
			// needed by snakeyaml
		}

		public Address(String streetName, String houseNumber, String zipCode, String cityName, String countryName) {
			this.streetName = streetName;
			this.houseNumber = houseNumber;
			this.zipCode = zipCode;
			this.cityName = cityName;
			this.countryName = countryName;
		}

		public String getStreetName() {
			return streetName;
		}

		public void setStreetName(String streetName) {
			this.streetName = streetName;
		}

		public String getHouseNumber() {
			return houseNumber;
		}

		public void setHouseNumber(String houseNumber) {
			this.houseNumber = houseNumber;
		}

		public String getZipCode() {
			return zipCode;
		}

		public void setZipCode(String zipCode) {
			this.zipCode = zipCode;
		}

		public String getCityName() {
			return cityName;
		}

		public void setCityName(String cityName) {
			this.cityName = cityName;
		}

		public String getCountryName() {
			return countryName;
		}

		public void setCountryName(String countryName) {
			this.countryName = countryName;
		}
	}

	public static class Supplier {

		public String name;
		public String id;
		public String apiURL;
		public int deliveryTime;
		public Address companyAddress;


		public Supplier() {
			// needed by snakeyaml
		}

		public Supplier(String name, String id, String apiURL, int deliveryTime, Address companyAddress) {
			this.name = name;
			this.id = id;
			this.apiURL = apiURL;
			this.deliveryTime = deliveryTime;
			this.companyAddress = companyAddress;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getApiURL() {
			return apiURL;
		}

		public void setApiURL(String apiURL) {
			this.apiURL = apiURL;
		}

		public int getDeliveryTime() {
			return deliveryTime;
		}

		public void setDeliveryTime(int deliveryTime) {
			this.deliveryTime = deliveryTime;
		}

		public Address getCompanyAddress() {
			return companyAddress;
		}

		public void setCompanyAddress(Address companyAddress) {
			this.companyAddress = companyAddress;
		}

	}

}
