package org.hso.ecommerce.entities.shop;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "categories")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic
    public long id;

    @NotNull
    @Column(unique = true)
    public String name;

    @ManyToMany(mappedBy = "categories")
    public Set<Article> articles = new HashSet<>();
    
    
    public Category() {
    	
    }
    
    
    public Category (String name) {
    	this.name = name;
    }
    
}
