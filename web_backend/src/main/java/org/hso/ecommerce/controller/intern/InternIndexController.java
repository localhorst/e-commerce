package org.hso.ecommerce.controller.intern;

import java.util.Optional;

import org.hso.ecommerce.controller.intern.accounting.AccountingController;
import org.hso.ecommerce.entities.booking.BookingAccountEntry;
import org.hso.ecommerce.repos.booking.BookingAccountEntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/intern")
public class InternIndexController {

    @Autowired
    private final BookingAccountEntryRepository bookingAccountEntryRepository = null;

    @GetMapping("/")
    public String intern(Model model) {
        Optional<BookingAccountEntry> mainAccount = bookingAccountEntryRepository.getByMain();
        int mainAccountBalance = mainAccount.map(entry -> entry.newSumCent).orElse(0);
        Optional<BookingAccountEntry> vatAccount = bookingAccountEntryRepository.getByVat();
        int vatAccountBalance = vatAccount.map(entry -> entry.newSumCent).orElse(0);

        model.addAttribute("mainAccountBalance", AccountingController.fmtEuro(mainAccountBalance));
        model.addAttribute("vatAccountBalance", AccountingController.fmtEuro(vatAccountBalance));

        return "intern/index";
    }

}
