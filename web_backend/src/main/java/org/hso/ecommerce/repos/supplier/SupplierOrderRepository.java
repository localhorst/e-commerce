package org.hso.ecommerce.repos.supplier;

import org.hso.ecommerce.entities.supplier.SupplierOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SupplierOrderRepository extends JpaRepository<SupplierOrder, Long> {

	@Query("SELECT SUM(so.numberOfUnits) FROM SupplierOrder so JOIN so.ordered ao WHERE ao.articleNumber = :articleNumber AND so.delivered IS NULL")
	Integer countUndeliveredReorders(String articleNumber);

	@Query(value = "SELECT * FROM supplier_orders as a WHERE a.supplier_id = :supplierId ORDER BY a.id DESC", nativeQuery = true)
	List<SupplierOrder> findOrderBySupplierID(@Param("supplierId") long supplierId);

	@Query("SELECT a FROM SupplierOrder a")
	List<SupplierOrder> findAll();

	@Query("SELECT a FROM SupplierOrder a ORDER BY a.id DESC")
	List<SupplierOrder> findAllDesc();
}
