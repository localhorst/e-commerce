package org.hso.ecommerce.entities.supplier;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "suppliers")
public class Supplier {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic
    public long id;

    @Column(unique = true)
    public String uuid;

    @NotNull
    public String name;

    @NotNull
    public String apiUrl;
}
