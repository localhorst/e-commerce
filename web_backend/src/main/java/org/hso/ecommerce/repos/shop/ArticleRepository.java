package org.hso.ecommerce.repos.shop;

import org.hso.ecommerce.entities.shop.Article;
import org.hso.ecommerce.entities.supplier.ArticleOffer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> {

    /***
     * use findById instead.
     */
    @Deprecated
    @Query("SELECT a FROM Article a WHERE a.id = :articleId")
    Article findArticleById(@Param("articleId") long articleId);

    @Query("SELECT a FROM Article a")
    List<Article> findAll();

    @Query(value = "SELECT DISTINCT a.* from articles as a, article_offers as ao, warehouse_booking_position_entries as wbpe where a.related_id = ao.id and wbpe.article_id = a.id and ao.should_be_advertised = true group by wbpe.slot_id having max(wbpe.id) and wbpe.new_sum_slot != 0", nativeQuery = true)
    List<Article> getAdvertisedArticles();

    @Query("SELECT a FROM CustomerOrderPosition cop JOIN cop.order co JOIN co.customer c JOIN cop.article a ORDER BY co.id DESC")
    List<Article> getOrderedArticles();

    @Query("SELECT DISTINCT a FROM CustomerOrderPosition cop JOIN cop.order co JOIN co.customer c JOIN cop.article a WHERE c.id = :customerId ORDER BY co.id DESC")
    List<Article> getOrderedArticles(long customerId);

    /***
     * use type safe findArticleByArticleOffer instead.
     */
    @Deprecated
    @Query(value = "SELECT a.id FROM articles a WHERE a.related_id = :relatedId", nativeQuery = true)
    Optional<Integer> findArticleIDByRelatedID(@Param("relatedId") long relatedId);

    @Query(value = "SELECT a FROM Article a Where a.related = :related")
    Optional<Article> findArticleByArticleOffer(ArticleOffer related);

    @Query(value = "Select a.* from articles as a, warehouse_booking_position_entries as wbpe where wbpe.article_id = a.id and a.title LIKE %:term% group by wbpe.slot_id having max(wbpe.id) and wbpe.new_sum_slot != 0", nativeQuery = true)
    List<Article> getArticlesByTermInTitle(String term);

    @Query(value = "Select a.* from articles as a, warehouse_booking_position_entries as wbpe where wbpe.article_id = a.id and a.description LIKE %:term% group by wbpe.slot_id having max(wbpe.id) and wbpe.new_sum_slot != 0", nativeQuery = true)
    List<Article> getArticlesByTermInDescription(String term);

    @Query(value = "Select distinct a.* from articles as a, categories as c, article_categories_bindings as acb, warehouse_booking_position_entries as wbpe where wbpe.article_id = a.id and acb.articles_id = a.id and acb.categories_id = c.id and c.name = :category group by wbpe.slot_id having max(wbpe.id) and wbpe.new_sum_slot != 0", nativeQuery = true)
    List<Article> getArticlesByCategory(String category);
}