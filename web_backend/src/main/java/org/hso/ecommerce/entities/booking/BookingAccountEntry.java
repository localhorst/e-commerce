package org.hso.ecommerce.entities.booking;

import org.hso.ecommerce.entities.supplier.Supplier;
import org.hso.ecommerce.entities.user.User;

import javax.persistence.*;

@Entity
@Table(name = "booking_account_entries")
public class BookingAccountEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic
    public long id;

    public int newSumCent;

    @ManyToOne(optional = true, cascade = CascadeType.MERGE)
    public User userAccount;

    @ManyToOne(optional = true, cascade = CascadeType.MERGE)
    public Supplier supplierAccount;

    public boolean isMainAccount;
    public boolean isVATAccount;

    public BookingAccountEntry copyAddAmount(int amountCent) {
        BookingAccountEntry e = new BookingAccountEntry();

        e.userAccount = userAccount;
        e.supplierAccount = supplierAccount;
        e.isMainAccount = isMainAccount;
        e.isVATAccount = isVATAccount;

        e.newSumCent = newSumCent + amountCent;

        return e;
    }

    public static BookingAccountEntry newUser(User user) {
        BookingAccountEntry e = new BookingAccountEntry();
        e.userAccount = user;
        e.newSumCent = 0;

        return e;
    }

    public static BookingAccountEntry newSupplier(Supplier supplier) {
        BookingAccountEntry e = new BookingAccountEntry();
        e.supplierAccount = supplier;
        e.newSumCent = 0;

        return e;
    }

    public static BookingAccountEntry newMain() {
        BookingAccountEntry e = new BookingAccountEntry();
        e.isMainAccount = true;
        e.newSumCent = 0;

        return e;
    }

    public static BookingAccountEntry newVat() {
        BookingAccountEntry e = new BookingAccountEntry();
        e.isVATAccount = true;
        e.newSumCent = 0;

        return e;
    }
}
