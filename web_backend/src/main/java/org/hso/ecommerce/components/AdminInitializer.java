package org.hso.ecommerce.components;

import org.hso.ecommerce.entities.shop.Address;
import org.hso.ecommerce.entities.user.User;
import org.hso.ecommerce.repos.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.util.Optional;

@Component
public class AdminInitializer {

	@Autowired
	private final UserRepository userRepository = null;

	@PostConstruct
	public void init() {
		Optional<Integer> numberOfEmployees = userRepository.numberOfEmployees();
		if (numberOfEmployees.orElse(0) == 0) {
			// create first admin user
			User firstAdmin = new User();
			firstAdmin.created = new Timestamp(System.currentTimeMillis());
			firstAdmin.defaultDeliveryAddress = new Address();
			firstAdmin.defaultDeliveryAddress.name = "admin";
			firstAdmin.defaultPayment = null;
			firstAdmin.email = "admin";
			firstAdmin.isActive = true;
			firstAdmin.isEmployee = true;
			firstAdmin.setPassword("admin");
			userRepository.save(firstAdmin); //save to DB
		}
	}
}
