package org.hso.ecommerce.supplier.data;

public class ReturnStatus {

    private String status;
    private String estimatedArrival;

    public ReturnStatus(String status, String estimatedArrival) {
        this.status = status;
        this.estimatedArrival = estimatedArrival;
    }

    public String getStatus() {
        return status;
    }

    public String getEstimatedArrival() {
        return estimatedArrival;
    }
}
