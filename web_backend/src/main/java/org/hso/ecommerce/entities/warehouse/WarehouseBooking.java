package org.hso.ecommerce.entities.warehouse;

import org.hso.ecommerce.entities.booking.BookingReason;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "warehouse_bookings")
public class WarehouseBooking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic
    public long id;

    @NotNull
    public java.sql.Timestamp created;

    public boolean isInProgress;
    public boolean isDone;

    @OneToMany(
            mappedBy = "booking", cascade = CascadeType.ALL
    )
    public List<WarehouseBookingPosition> positions = new ArrayList<>();

    // TODO FIX ME
    @OneToOne(optional = false, cascade = CascadeType.ALL)
    public BookingReason reason;
}
